import "@splidejs/splide/css";
import Splide from "@splidejs/splide";

document.addEventListener("DOMContentLoaded", function () {
  var customerReviewsSlider = new Splide("#customerReviewsSlider", {
    type: "loop",
    perPage: 4,
    perMove: 1,
    gap: "0.25rem",
    interval: 2000,
    autoplay: true,
    pauseOnFocus: true,
    pauseOnHover: false,
    arrows: false,
    pagination: false,
    breakpoints: {
      640: {
        perPage: 1,
      },
      960: {
        perPage: 2,
      },
    }
  });
  customerReviewsSlider.mount();
  var customerReviewsSlider = new Splide("#projectsSlider", {
    type: "loop",
    perPage: 4,
    perMove: 1,
    gap: "0.25rem",
    interval: 2000,
    autoplay: true,
    pauseOnFocus: true,
    pauseOnHover: false,
    arrows: false,
    pagination: false,
    breakpoints: {
      640: {
        perPage: 1,
      },
      960: {
        perPage: 2,
      },
    }
  });
  customerReviewsSlider.mount();
});
