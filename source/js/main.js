import bootstrap from "bootstrap/dist/js/bootstrap.bundle.js";
import OverlayScrollbars from "overlayscrollbars";

import "./images";
import "../scss/main.scss";

document.addEventListener("DOMContentLoaded", function () {
  // BOOTSTRAP TOOLTIP
  var tooltipTriggerList = [].slice.call(
    document.querySelectorAll('[data-bs-toggle="tooltip"]')
  );
  tooltipTriggerList.map(function (tooltipTriggerEl) {
    return new bootstrap.Tooltip(tooltipTriggerEl);
  });

  // BOOTSTRAP POPOVER
  var popoverTriggerList = [].slice.call(
    document.querySelectorAll('[data-bs-toggle="popover"]')
  );
  popoverTriggerList.map(function (popoverTriggerEl) {
    let opts = {
      animation: true,
    };
    if (popoverTriggerEl.hasAttribute("data-bs-content-id")) {
      var content_id = popoverTriggerEl.getAttribute("data-bs-content-id");
      var content_el = document.getElementById(content_id);
      if (content_el != null) {
        opts.content = content_el.innerHTML;
      } else {
        opts.content = `content element with #${content_id} not found!`;
      }
      opts.html = true;
    }
    return new bootstrap.Popover(popoverTriggerEl, opts);
  });

  var OverlayScrollbarsList = [].slice.call(
    document.querySelectorAll(".scrollbars")
  );
  OverlayScrollbarsList.map(function (scrollbarsEl) {
    return new OverlayScrollbars(scrollbarsEl, {});
  });
});

window.bootstrap = bootstrap;
window.OverlayScrollbars = OverlayScrollbars;
