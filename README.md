# 🚀 Welcome to your new awesome project

This project has been created using **webpack-cli**, you can now run

```shell
npm run build
```

or

```shell
yarn build
```

to bundle your application

```shell
npm run build
```
